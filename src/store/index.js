import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
    currentUser: {
      isAuthUser: false,
      name: '',
    },
    discounts: [],
    promotions: [],
    currentDiscount: {},
    currentDiscountReviews: [],
    currentPromotion: {},
    orders: [],
  },
  getters: {
    currentUser(state) {
      return state.currentUser;
    },
    discountsList(state) {
      return state.discounts;
    },
    promotionsList(state) {
      return state.promotions;
    },
    currentDiscount(state) {
      return state.currentDiscount;
    },
    currentDiscountReviews(state) {
      return state.currentDiscountReviews;
    },
    ordersList(state) {
      return state.orders;
    },
  },
  mutations: {
    toggleAuthUser(state, payload) {
      state.currentUser.isAuthUser = !state.currentUser.isAuthUser;
    },
    setDiscounts(state, payload) {
      state.discounts = payload;
    },
    setPromotions(state, payload) {
      state.promotions = payload;
    },
    setCurrentDiscount(state, payload) {
      state.currentDiscount = payload;
    },
    setCurrentPromotion(state, payload) {
      state.currentPromotion = payload;
    },
    setCurrentDiscountReviews(state, payload) {
      state.currentDiscountReviews = payload;
    },
    setOrders(state, payload) {
      state.orders = payload;
    },
  },
  actions: {
    async getDiscounts(context) {
      const response = await axios.get('http://localhost:3006/discounts');
      const data = await response.data
      context.commit('setDiscounts', data);
    },
    async getPromotions(context) {
      const response = await axios.get('http://localhost:3006/promotions');
      const data = await response.data
      context.commit('setPromotions', data);
    },
    async getCurrentDiscount(context, id) {
      const response = await axios.get(`http://localhost:3006/discounts/${id}`);
      const data = await response.data
      context.commit('setCurrentDiscount', data);
    },
    async getCurrentPromotion(context, id) {
      const response = await axios.get(`http://localhost:3006/promotions/${id}`);
      const data = await response.data
      context.commit('setCurrentPromotion', data);
    },
    async postReview(context, review) {
      const response = await axios.post('http://localhost:3006/reviews', review);
      const data = await response.data;
      console.log('Ответ от сервера: ', data)
    },
    async getCurrentDiscountReviews(context, id) {
      const response = await axios.get(`http://localhost:3006/reviews?product_id=${id}`);
      const data = await response.data
      context.commit('setCurrentDiscountReviews', data);
    },
    async postOrder(context, order) {
      const response = await axios.post('http://localhost:3006/orders', order);
      const data = await response.data;
      console.log('Ответ от сервера: ', data)
    },
    async getOrders(context, order) {
      const response = await axios.get('http://localhost:3006/orders');
      const data = await response.data
      context.commit('setOrders', data);
    },
  },
  modules: {
  }
})
